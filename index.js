var openpgp = require('openpgp');
var stream = require('stream');

pgp = {};

pgp.key = function (options) {
    return new stream.Readable({
        objectMode: true,
        read: function () {
            openpgp.key.generate(options).then(function(key) {
                this.push(key);
                this.push(null);
            }.bind(this));
        }
    });
};

pgp.key.pipe = function (destination) {
    return new stream.PassThrough({
        objectMode: true,
        transform: function (chunk, encoding, callback) {
            callback(null, chunk.toPublic());
        }
    }).on('pipe', function(source) {
        this.pipe(destination);
    });
};

pgp.armor = {};

pgp.armor.pipe = function (destination) {
    return new stream.PassThrough({
        objectMode: true,
        transform: function (chunk, encoding, callback) {
            callback(null, chunk.armor());
        }
    }).on('pipe', function(source) {
        this.pipe(destination);
    });
}
