['./index.js', 'unit'].map(require);

unit = unit(exports);

var options = {
    numBits: 512,
    userId: 'Jon Smith <jon.smith@example.org>',
    passphrase: 'super long and hard to guess secret'
};

pgp.key(options).pipe(
    unit(function(key) { return key.isPrivate(); })
);

pgp.key(options).pipe(
    pgp.armor.pipe(
        unit(/-----BEGIN PGP PRIVATE KEY BLOCK-----/)
    )
);

pgp.key(options).pipe(
    pgp.key.pipe(
        unit(function(key) { return key.isPublic(); })
    )
);

pgp.key(options).pipe(
    pgp.key.pipe(
        pgp.armor.pipe(
            unit(/-----BEGIN PGP PUBLIC KEY BLOCK-----/)
        )
    )
);
